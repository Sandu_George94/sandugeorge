package abc;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Tasta 1 pentru a introduce de la tastatura.");
		System.out.println("Tasta 2 pentru numere random.");
		System.out.println("Tasta 3 pentru a iesi.");
		
		Scanner optiune = new Scanner(System.in);
		int opt = optiune.nextInt();
		boolean x = true;
		while(x){
			
			switch (opt) {
			case 1:
				Scanner in = new Scanner(System.in);
				
				ArrayList<Integer> sirNumere = new ArrayList<Integer>();
				System.out.println("Introduceti numerele de la tastatura si tastati 'Ok' cand ati terminat.");
				while(in.hasNextInt()){
					sirNumere.add(in.nextInt());
				}
				System.out.println(sirNumere);
				maxim(sirNumere);
				minim(sirNumere);
				palindrom(sirNumere);
				prim(sirNumere);
				//in.close();
				break;
				
			case 2:
				System.out.println("Introduceti lungimea sirului.");
				Scanner lungime = new Scanner(System.in);
				Integer n = lungime.nextInt();
				ArrayList<Integer> sirNumereRandom = new ArrayList<Integer>(n);
				
				Random rand = new Random();
				rand.setSeed(System.currentTimeMillis());
				int r;
				for(int i=0;i<n;i++){
		            r = rand.nextInt() % 256;
		            sirNumereRandom.add(r);					
				}
				
				System.out.println(sirNumereRandom);
				
				maxim(sirNumereRandom);
				minim(sirNumereRandom);
				palindrom(sirNumereRandom);
				prim(sirNumereRandom);
				//lungime.close();
				break; 
				
			case 3:
				System.out.println("Sa aveti o zi buna! :D");
				x=false;
				break;
				
			default:
				System.out.println("Introduceti 1, 2 sau 3.");
				break;
				
			}
			
			if(x){
				
				System.out.println("\n\nTasta 1 pentru a introduce de la tastatura.");
				System.out.println("Tasta 2 pentru numere random.");
				System.out.println("Tasta 3 pentru a iesi.");
				opt = optiune.nextInt();
			}
		}
		//optiune.close();
		
		
	}
	
	
	public static void maxim(ArrayList<Integer> sirNumere){
		int max = sirNumere.get(0);
		for(int i=1;i<sirNumere.size();i++){
			if(max<sirNumere.get(i))
				max = sirNumere.get(i);
		}
		System.out.println("Numarul maxim din sir este " + max);
	}
	
	public static void minim(ArrayList<Integer> sirNumere){
		int min = sirNumere.get(0);
		for(int i=1;i<sirNumere.size();i++){
			if(min>sirNumere.get(i))
				min = sirNumere.get(i);
		}
		System.out.println("Numarul minim din sir este " + min);
	}
	
	public static boolean isPalindrom(char[] sir){
		int i1 = 0;
	    int i2 = sir.length - 1;
	    while (i2 > i1) {
	        if (sir[i1] != sir[i2]) {
	            return false;
	        }
	        ++i1;
	        --i2;
	    }
	    return true;
	}
	
	public static void palindrom(ArrayList<Integer> sirNumere){
		ArrayList<Integer> sirNumerePalindrom = new ArrayList<Integer>();
		
		for(int i=0;i<sirNumere.size();i++){
			String a = sirNumere.get(i).toString();
			char[] b = a.toCharArray();
			if(isPalindrom(b) && sirNumere.get(i)>10)
				sirNumerePalindrom.add(sirNumere.get(i));
		}
		if(!sirNumerePalindrom.isEmpty()){
			System.out.print("Palindromes: ");
			for(int i=0;i<sirNumerePalindrom.size();i++)
				System.out.print(sirNumerePalindrom.get(i) + " ");
		}
	}
	
	public static boolean isNrPrim(int a){
	    if (a%2==0) return false;
	    for(int i=3;i*i<=a;i+=2) {
	        if(a%i==0)
	            return false;
	    }
	    return true;
	}
	
	public static void prim(ArrayList<Integer> sirNumere){
		ArrayList<Integer> sirNumerePrime = new ArrayList<Integer>();
		
		for(int i=0; i<sirNumere.size(); i++){
			if(isNrPrim(sirNumere.get(i)) && sirNumere.get(i)>0)
				sirNumerePrime.add(sirNumere.get(i));					
		}
		
		if(!sirNumerePrime.isEmpty()){
			System.out.print("\nPrime numbers: ");
			for(int i=0;i<sirNumerePrime.size();i++)
				System.out.print(sirNumerePrime.get(i) + " ");
		}
	}
}
